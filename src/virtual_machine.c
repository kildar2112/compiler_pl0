// Randy Glasgow
// Nick Plympton
// COP 3402: Spring 2020

#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define MAX_DATA_STACK_HEIGHT 40
#define MAX_CODE_LENGTH 200
#define MAX_REGISTER_SIZE 8
#define MAX_LEX_LEVELS 3

typedef struct global_constraints
{
    int SP;   // Stack Pointer:
    int BP;   // Base Pointer
    int PC;   // Program Counter
    int IR;   // Instruction Register
    int HALT; // Global Halt FLAG
} global_constants;
global_constants gc; // Global Constants

typedef struct instruct_reg
{
    int op; // Operation Code
    int r;  // Register Index
    int l;  // Lexicographical Level
    int m;  // Stored Val or something.
    struct instruct_reg *next;
} instruction;

instruction CODE[MAX_CODE_LENGTH]; // Head of the Instruction Register
instruction CURRENT;               // Current Node

int REGISTER[8] = {0};                  // Register for computation
int ACTUAL_CODE_LENGTH = 0;             // The actual code length after its been read in
int STACK[MAX_DATA_STACK_HEIGHT] = {0}; // Stack for the logic and arithmetic

const char *op_code[] = {
    // Operation Codes
    "lit", // op - 1 = 0
    "rtn", // 1
    "lod", // 2
    "sto", // 3
    "cal", // 4
    "inc", // 5
    "jmp", // 6
    "jpc", // 7
    "sio", // 8
    "sio", // 9
    "sio", // 10
    "neg", // 11
    "add", // 12
    "sub", // 13
    "mul", // 14
    "div", // 15
    "odd", // 16
    "mod", // 17
    "eql", // 18
    "neq", // 19
    "lss", // 20
    "leq", // 21
    "gtr", // 22
    "geq"  // 23
};

int base(int l, int base)
{
    int b1;
    b1 = base;
    while (l > 0)
    {
        b1 = STACK[b1 - 1];
        l--;
    }
    return b1;
}
void fetch_cycle()
{
    CURRENT = CODE[gc.PC++];
}
void execute_cycle()
{
    switch (CURRENT.op)
    {
    case 1: // LIT
        REGISTER[CURRENT.r] = CURRENT.m;
        break;

    case 2: // RTN
        gc.SP = gc.BP - 1;
        gc.BP = STACK[gc.SP + 3];
        gc.PC = STACK[gc.SP + 4];
        break;

    case 3: // LOD
        REGISTER[CURRENT.r] = STACK[base(CURRENT.l, gc.BP) + CURRENT.m];
        break;

    case 4: // STO
        STACK[base(CURRENT.l, gc.BP) + CURRENT.m] = REGISTER[CURRENT.r];
        break;

    case 5: // CAL
        STACK[gc.SP + 1] = 0;
        STACK[gc.SP + 2] = base(CURRENT.l, gc.BP);
        STACK[gc.SP + 3] = gc.BP;
        STACK[gc.SP + 4] = gc.PC;
        gc.BP = gc.SP + 1;
        gc.PC = CURRENT.m;
        break;

    case 6: // INC
        gc.SP = gc.SP + CURRENT.m;
        break;

    case 7: // JMP
        gc.PC = CURRENT.m;
        break;

    case 8: // JPC
        if (REGISTER[CURRENT.r] == 0)
        {
            gc.PC = CURRENT.m;
        }
        break;

    case 9: // SIO
        printf("%d", REGISTER[CURRENT.r]);
        break;

    case 10: // SIO 2
        scanf("%d", &REGISTER[CURRENT.r]);
        break;

    case 11: // SIO 3
        gc.HALT = 1;
        break;

    case 12: // NEG
        REGISTER[CURRENT.r] = -REGISTER[CURRENT.r];
        break;

    case 13: // ADD
        REGISTER[CURRENT.r] = REGISTER[CURRENT.l] + REGISTER[CURRENT.m];
        break;

    case 14: // SUB
        REGISTER[CURRENT.r] = REGISTER[CURRENT.l] - REGISTER[CURRENT.m];
        break;

    case 15: // MUL
        REGISTER[CURRENT.r] = REGISTER[CURRENT.l] * REGISTER[CURRENT.m];
        break;

    case 16: // DIV
        REGISTER[CURRENT.r] = REGISTER[CURRENT.l] / REGISTER[CURRENT.m];
        break;

    case 17: // ODD
        REGISTER[CURRENT.r] = REGISTER[CURRENT.r] % 2;
        break;

    case 18: // MOD
        REGISTER[CURRENT.r] = REGISTER[CURRENT.l] % REGISTER[CURRENT.m];
        break;

    case 19: // EQL
        REGISTER[CURRENT.r] = REGISTER[CURRENT.l] == REGISTER[CURRENT.m];
        break;

    case 20: // NEQ
        REGISTER[CURRENT.r] = REGISTER[CURRENT.l] != REGISTER[CURRENT.m];
        break;

    case 21: // LSS
        REGISTER[CURRENT.r] = REGISTER[CURRENT.l] < REGISTER[CURRENT.m];
        break;

    case 22: // LEQ
        REGISTER[CURRENT.r] = REGISTER[CURRENT.l] <= REGISTER[CURRENT.m];
        break;

    case 23: // GTR
        REGISTER[CURRENT.r] = REGISTER[CURRENT.l] > REGISTER[CURRENT.m];
        break;

    case 24: // GEQ
        REGISTER[CURRENT.r] = REGISTER[CURRENT.l] >= REGISTER[CURRENT.m];
        break;

    default:

        printf("Error: Unkown Operation Code | %d\n", CURRENT.op);
        break;
    }
}

// Re Write the read in
void read_in_code(char const *filename)
{
    FILE *ifp = fopen(filename, "r");
    if (ifp)
    {
        int index;
        for (index = 0; index < MAX_CODE_LENGTH && !feof(ifp); index++)
        {
            if (index < MAX_CODE_LENGTH)
            {
                fscanf(ifp, "%d %d %d %d", &CODE[index].op, &CODE[index].r, &CODE[index].l, &CODE[index].m);
                ACTUAL_CODE_LENGTH++;
            }
            else
            {
                // Exceeding the max code length allowed
                printf("Error: Maximum Code Length Exceeded | %d", index);
            }
        }
    }
}

void print_out_code()
{
    int index = 0;
    printf("Line:\t OP  R  L  M\n");
    for (index = 0; index < ACTUAL_CODE_LENGTH - 1; index++)
    {
        printf("%4d:\t%2s %2d %2d  %-3d\n", index, op_code[CODE[index].op - 1], CODE[index].r, CODE[index].l, CODE[index].m);
    }
}

void print_initial_stack()
{
    // Print out current stack
    int i;
    printf("%25s%10s%10s%20s\n", "PC", "BP", "SP", "REGISTER");

    printf("%15s%10d%10d%10d\t\t ", "Initial Values:", gc.PC, gc.BP, gc.SP);
    for (i = 0; i < MAX_REGISTER_SIZE; i++)
    {
        printf("%d", REGISTER[i]);
    }
    int j;
    printf("\nSTACK:\t");
    for (j = 0; j < MAX_DATA_STACK_HEIGHT; j++)
    {
        printf("%d", STACK[j]);
    }
    printf("\n");
}
void print_execution_stack()
{
    printf("---------------------------------------------------------------------------------------\n");

    printf("%25s%10s%10s%20s\n", "PC", "BP", "SP", "REGISTER");
    printf("%5d:%5s%3d%3d%3d:", gc.PC, op_code[CURRENT.op - 1], CURRENT.r, CURRENT.l, CURRENT.m);
    printf("%4d%10d%10d\t\t ", gc.PC, gc.BP, gc.SP);
    int i;
    for (i = 0; i < MAX_REGISTER_SIZE; i++)
    {
        printf("%d", REGISTER[i]);
    }
    printf("\n");
    printf("%-6s\t", "STACK:");
    int j;
    for (j = 0; j < MAX_DATA_STACK_HEIGHT; j++)
    {
        printf("%-1d", STACK[j]);
    }
    printf("\n");
}

int main(int argc, char const *argv[])
{
    gc.BP = 1;
    gc.SP = 0;
    gc.PC = 0;
    gc.IR = 0;
    gc.HALT = 0;

    read_in_code(argv[1]);
    print_out_code();

    print_initial_stack();

    CURRENT = CODE[0];
    int index;
    for (index = 0; index < ACTUAL_CODE_LENGTH - 2 || gc.HALT != 1; index++)
    {
        execute_cycle();
        print_execution_stack();
        fetch_cycle();
    }

    return 0;
}
